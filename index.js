const Express = require('express');
const app = Express();
const mongoose = require('mongoose')

mongoose.connect(process.env.DB_CONNECTION, {
  useUnifiedTopology: true,
  useNewUrlParser: true
});

const mainController = require('./src/controllers')

app.disable('etag');
app.use(Express.json());
mainController(app);

app.get('/ping', (req, res) => res.send('pong'))

const port = 5001;
app.listen(port, () => {
  console.log(`[ls-backend] - app runs on port ${port}`);
})