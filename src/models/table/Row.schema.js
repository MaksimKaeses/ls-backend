const mongoose = require("mongoose");

const rowSchema = mongoose.Schema({
  id: {
    type: String,
    required: true
  },
  firstName: {
    type: String,
    required: true
  },
  secondName: {
    type: String,
    required: true
  },
  comment: {
    type: String,
  }
});

module.exports = rowSchema;