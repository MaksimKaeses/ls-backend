const mongoose = require("mongoose");
const rowSchema = require('./Row.schema')

const tableSchema = mongoose.Schema({
  apiKey: {
    type: String,
    required: true
  },
  data: {
    type: [rowSchema],
    default: []
  }
});

module.exports = mongoose.model("Table", tableSchema);