const User = require('../models/User.model');
const Table = require('../models/table/Table.model');

const { tableDataValidate } = require('../validate');
const generateKey = require('../utils/generateId');

const insertData = async (req, res) => {
  const { error } = tableDataValidate(req.body);
  if (error) return res.status(422).send({ error: error.details[0].message });

  try {
    const userWithKey = await User.findOne({ apiKey: res.locals.apiKey });
    if (!userWithKey) return res.status(422).send({ error: "No such api key has been found" });

    const newRow = {
      id: generateKey(),
      firstName: req.body.firstName,
      secondName: req.body.secondName,
      comment: req.body.comment
    };

    const tableWithKey = await Table.findOne({ apiKey: res.locals.apiKey });
    tableWithKey.data.push(newRow)
    await tableWithKey.save();

    return res.status(200).send(newRow);
  } catch (error) {
    console.error(error)
    return res.status(500).send({ error })
  }
}

const removeData = async (req, res) => {
  if (!req.body.id) return res.status(422).send({ error: "No id has been given" })
  try {
    const tableWithKey = await Table.findOne({ apiKey: res.locals.apiKey });
    if (!tableWithKey) return res.status(422).send({ error: "No such api key has been found" });

    const filteredTableData = tableWithKey.data.filter(item => item.id !== req.body.id);
    tableWithKey.data = filteredTableData;
    tableWithKey.save();

    return res.status(200).send()
  } catch (error) {
    console.error(error);
    return res.status(500).send({ error })
  }
}

const getData = async (req, res) => {
  if (!res.locals.apiKey) return res.status(422).send({ error: 'No api key has been given' });

  try {
    const tableWithKey = await Table.findOne({ apiKey: res.locals.apiKey });
    if (!tableWithKey) return res.status(422).send({ error: "No such api key has been found" });

    return res.status(200).send({ data: tableWithKey.data })
  } catch (error) {
    console.error(error);
    return res.status(500).send({ error })
  }
}

module.exports = {
  insertData,
  removeData,
  getData
}