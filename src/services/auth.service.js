const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const { authValidate } = require('../validate/');
const User = require('../models/User.model');
const Table = require('../models/table/Table.model');
const generateKey = require('../utils/generateId')


const signUp = async (req, res) => {
  try {
    const isRegistrated = await User.findOne({ email: req.body.email });
    if (isRegistrated)
      return res.status(400).send({ error: "User with this email already exists" });
  } catch (error) {
    console.error(error);
    return res.status(500).send({ error })
  }

  const { error } = authValidate(req.body);
  if (error) return res.status(422).send({ error: error.details[0].message });

  const newUser = new User({
    email: req.body.email,
    password: crypto.createHash('md5').update(req.body.password + process.env.SALT).digest("hex"),
    apiKey: generateKey()
  });

  const newTable = new Table({
    apiKey: newUser.apiKey
  })

  try {
    await newUser.save();
    await newTable.save();

    return res.status(201).send({ message: "Success" })
  } catch (error) {
    console.error(error);
    return res.status(500).send(error)
  }
}

const signIn = async (req, res) => {
  const { error } = authValidate(req.body);
  if (error)
    return res.status(422).send({ error: error.details[0].message });

  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
      return res.status(400).send({ error: "Wrong email address" });
    } else {
      if (crypto.createHash("md5").update(req.body.password + process.env.SALT).digest("hex") !== user.password)
        return res.status(400).send({ error: "Incorrect credentials" });

      const token = jwt.sign({ apiKey: user.apiKey }, process.env.SECRET, {
        expiresIn: "24h"
      });

      return res.status(200).send({
        email: user.email,
        apiKey: user.apiKey,
        token
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).send(error)
  }
}

const getUser = async (req, res) => {
  const { apiKey } = res.locals;

  try {
    const user = await User.findOne({ apiKey });
    if (!user) throw new Error('Something went wrong');

    return res.status(200).send({ email: user.email, apiKey: user.apiKey })
  } catch (error) {
    console.error(error);
    return res.status(500).send({ error })
  }
}

module.exports = {
  signUp,
  signIn,
  getUser
}