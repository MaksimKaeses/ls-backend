const Joi = require('@hapi/joi');

const authValidate = data => {
  const schema = Joi.object({
    email: Joi.string().required().email(),
    password: Joi.string().min(6).required()
  })

  return schema.validate(data);
}

const tableDataValidate = data => {
  const schema = Joi.object({
    firstName: Joi.string().required().min(2),
    secondName: Joi.string().required().min(2),
    comment: Joi.string().required().min(6).max(100)
  });

  return schema.validate(data);
}

module.exports = {
  authValidate,
  tableDataValidate
}