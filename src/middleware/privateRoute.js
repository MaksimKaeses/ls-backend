const jwt = require('jsonwebtoken');

const privateRoute = (req, res, next) => {
  const header = req.headers.authorization && req.headers.authorization.split(" ");

  if (!header || header[0] !== "Bearer")
    return res.status(401).send({ error: "Unauthorized" });

  try {
    const decoded = jwt.verify(header[1], process.env.SECRET);
    if (!decoded || decoded.error)
      return res.status(401).send({ error: "Unauthorized" });

    res.locals.apiKey = decoded.apiKey
    next();
  } catch (e) {
    return res.status(401).send({ error: "Unauthorized", ...e });
  }
}

module.exports = privateRoute;