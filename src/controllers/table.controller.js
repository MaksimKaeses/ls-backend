const router = require('express').Router();
const { insertData, removeData, getData } = require('../services/table.service');

const privateRoute = require('../middleware/privateRoute')

router.post('/', privateRoute, (req, res) => insertData(req, res));
router.delete('/', privateRoute, (req, res) => removeData(req, res));
router.get('/', privateRoute, (req, res) => getData(req, res));

module.exports = router;