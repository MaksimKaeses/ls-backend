const authController = require('./auth.controller');
const tableController = require('./table.controller');

const mainController = app => {
  app.use('/api/auth', authController);
  app.use('/api/table', tableController)
}

module.exports = mainController;