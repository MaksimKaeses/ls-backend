const router = require('express').Router();

const privateRoute = require('../middleware/privateRoute')
const { signUp, signIn, getUser } = require('../services/auth.service');

router.post('/sign-up', (req, res) => signUp(req, res));
router.post('/sign-in', (req, res) => signIn(req, res));
router.get('/user', privateRoute, (req, res) => getUser(req, res));
router.get('/test', privateRoute, (req, res) => res.send('success'))

module.exports = router;